﻿using QuanLyLichNhacViec.models;
using Syncfusion.WinForms.Input.Events;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using QuanLyLichNhacViec.DAO;
using Syncfusion.WinForms.Input;

namespace QuanLyLichNhacViec
{
    public partial class mainFrm : Form
    {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance();
        List<TaskCal> task = new List<TaskCal>();
        List<TaskCal> orginList = new List<TaskCal>();
        List<TaskCal> currentList = new List<TaskCal>();
        TaskDAOImpl taskDAOImpl = new TaskDAOImpl();
        DateTime currentDate = DateTime.Today;
        bool isPrioritize = false;
        bool isAllTask = true;
        bool isOpen = true;
        public mainFrm()
        {
            InitializeComponent();
            initializeData();
            initializeListView();
            initStatus();
            // specialDate();
            // showNotification();
        }


        //private void specialDate()
        //{
        //    List<SpecialDate> SpecialDates = new List<SpecialDate>();
        //    foreach (TaskCal val in task)
        //    {
        //        if(!(SpecialDates.Count(d => d.Value == val.createAt)>1))
        //        {
        //            SpecialDate specialDate = new SpecialDate();
        //            specialDate.BackColor = System.Drawing.Color.LightGray;
        //            specialDate.Value = val.createAt;
        //            SpecialDates.Add(specialDate);
        //        }


        //    }

        //    sfCalendar2.SpecialDates = SpecialDates;
        //}


        public void initStatus()
        {
            laTaskStatus.Text = "Tổng: " + task.Count;
            int doneCount = this.task.Count(p => p.state == "DONE");
            laTaskDone.Text = "Hoàn thành: " + doneCount;
            int doingCount = this.task.Count(p => p.state == "DOING");
            laTaskDoing.Text = "Đang làm: " + doingCount;
            int pedding = this.task.Count(p => p.state == "PENDDING");
            laTaskPendding.Text = "Chưa làm: " + pedding;


        }


        private void initializeListView()
        {
            lvTask.Items.Clear();
            lvTask.View = View.Details;
            currentList.Clear();
            foreach (TaskCal val in task)
            {
                if (!isAllTask)
                {
                    if (val.createAt == currentDate)
                    {

                        ListViewItem item = new ListViewItem(val.toRow());
                        if (val.state == "PENDDING")
                        {
                            item.ForeColor = Color.Red;
                        }
                        else if (val.state == "DONE")
                        {
                            item.ForeColor = Color.Green;
                        }
                        else
                        {
                            item.ForeColor = Color.Blue;
                        }

                        lvTask.Items.Add(item);
                        currentList.Add(val);
                    }
                }
                else
                {
                    ListViewItem item = new ListViewItem(val.toRow());

                    if (val.state == "PENDDING")
                    {
                        item.ForeColor = Color.Red;
                    }
                    else if (val.state == "DONE")
                    {
                        item.ForeColor = Color.Green;
                    }
                    else
                    {
                        item.ForeColor = Color.Blue;
                    }
                    lvTask.Items.Add(item);
                    currentList.Add(val);
                }

            }

        }

        private void initializeData()
        {
            task.Clear();
            task = taskDAOImpl.getAllTasks();
            orginList = taskDAOImpl.getAllTasks();


        }

        private void sfbtnToday_Click(object sender, EventArgs e)
        {
            isAllTask = false;
            // sfCalendar1.SelectedDate = DateTime.Today;
            //sfCalendar2.SelectedDate = DateTime.Today;
            initializeListView();

        }

        private void btnAddTask_Click(object sender, EventArgs e)
        {

            CreateFrm createFrm = new CreateFrm((_) =>
            {
                initializeData();
                initializeListView();
            }
            );
            //databaseHelper.closeConnection();
            createFrm.Show();




        }


        private void sfCalendar1_SelectionChanged(Syncfusion.WinForms.Input.SfCalendar sender, SelectionChangedEventArgs e)
        {
            var newDate = e.NewValue;
            currentDate = newDate.Value;
            //initializeData();
            isAllTask = false;
            initializeListView();
        }

        private void btnAllTask_Click(object sender, EventArgs e)
        {
            isAllTask = true;
            clearAllConditions();
            initializeData();
            initializeListView();
        }

        private void clearAllConditions()
        {
            this.conditions.Keys.ToList().ForEach(x => this.conditions[x] = false);
            chkbDone.Checked = false;
            chkbDoing.Checked = false;
            chkbPendding.Checked = false;
            chkbHigh.Checked = false;
            chkbMedium.Checked = false;
            chkbLow.Checked = false;
        }

        private void lvTask_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            String ID = lvTask.SelectedItems[0].Text;
            TaskCal val = getTaskFormID(ID);
            CreateFrm createFrm = new CreateFrm(val, (_) =>
            {
                initializeData();
                initializeListView();
            });
            //databaseHelper.closeConnection();
            createFrm.Show();
        }

        private TaskCal getTaskFormID(string id)
        {

            return task.Single(t => t.id.ToString().Equals(id));
        }

        private void mouse_click(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (lvTask.FocusedItem != null && lvTask.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    ContextMenu m = new ContextMenu();
                    MenuItem cashMenuItem = new MenuItem("Xóa công việc");
                    cashMenuItem.Click += delegate (object sender2, EventArgs e2)
                    {
                        String ID = lvTask.SelectedItems[0].Text;
                        DeleteTask(ID);
                    };// your action here 
                    m.MenuItems.Add(cashMenuItem);

                    MenuItem cashMenuItem2 = new MenuItem("-");
                    m.MenuItems.Add(cashMenuItem2);

                    MenuItem delMenuItem = new MenuItem("Cập nhật công việc");
                    delMenuItem.Click += delegate (object sender2, EventArgs e2)
                    {
                        String ID = lvTask.SelectedItems[0].Text;
                        TaskCal val = getTaskFormID(ID);
                        CreateFrm createFrm = new CreateFrm(val, (_) =>
                        {
                            initializeData();
                            initializeListView();
                        });

                        createFrm.Show();
                    };// your action here
                    m.MenuItems.Add(delMenuItem);

                    m.Show(lvTask, new Point(e.X, e.Y));

                }
            }
        }


        private void DeleteTask(string ID)
        {
            TaskCal val = getTaskFormID(ID);
            TaskDAOImpl taskDAOImpl = new TaskDAOImpl();
            taskDAOImpl.deleteTask(val);
            initializeData();
            initializeListView();
        }

        private void lvTask_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void showNotification()
        {


            if (isOpen)
            {
                int countCreateAtToday = this.task.Count(p => p.createAt == DateTime.Today);
                int countDueAtToday = this.task.Count(p => p.dueAt == DateTime.Today);

                string noitification = string.Format(" Bạn có {0} công việc bắt đầu vào ngày hôm nay.\n Bạn có {1} công việc cần hoàn thành trong hôm nay", countCreateAtToday, countDueAtToday);
                MessageBox.Show(noitification, "Lưu ý");
                isOpen = false;
            }

        }

        private void formLoad(object sender, EventArgs e)
        {
            showNotification();
            SizeLastColumn(lvTask);
        }

        private void lvTask_Resize(object sender, System.EventArgs e)
        {
            SizeLastColumn((ListView)sender);
        }

        private void SizeLastColumn(ListView lv)
        {
            int x = lv.Width / 26 == 0 ? 1 : lv.Width / 26;
            lv.Columns[0].Width = x;
            lv.Columns[1].Width = x * 4;
            lv.Columns[2].Width = x * 6;
            lv.Columns[3].Width = x * 4;
            lv.Columns[4].Width = x * 4;
            lv.Columns[5].Width = x * 4;
            lv.Columns[6].Width = x * 3;

        }


        private void btnExportTXT_Click(object sender, EventArgs e)
        {

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.DefaultExt = "txt";
            saveFileDialog1.Filter = "Text file (*.txt)|*.txt|XML file (*.xml)|*.xml|All files (*.*)|*.*";
            saveFileDialog1.AddExtension = true;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.Title = "Bạn muốn lưu tập tin ở đâu?";
            saveFileDialog1.InitialDirectory = @"C:/";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {

                ExportImport exportImport = new ExportImport();
                exportImport.export(saveFileDialog1.FileName, this.currentList);
                MessageBox.Show("Xuất danh sách thành công");

            }
            else
            {
                MessageBox.Show("Thao tác đã được hủy");
            }
            saveFileDialog1.Dispose();
            saveFileDialog1 = null;
        }

        private void btnImportTXT_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng chưa hoàn thiện!!!!");
        }

        private void pnListTask_Paint(object sender, PaintEventArgs e)
        {

        }

        Dictionary<string, bool> conditions = new Dictionary<string, bool> {
            { "done",false},
            { "pendding",false},
            { "doing",false},
            {"high",false},
            { "medium",false },
            { "low",false}
        };

        private void filter()
        {
            List<TaskCal> val = new List<TaskCal>();

            bool isFilter = conditions.All(kvp => !kvp.Value);
            if (isFilter)
            {
                initializeData();
                initializeListView();
                return;
            }

            foreach (KeyValuePair<string, bool> con in conditions)
            {

                List<TaskCal> temp = new List<TaskCal>();
                foreach (TaskCal t in orginList)
                {
                    if(con.Key.Equals("done") ||
                        con.Key.Equals("doing") || 
                        con.Key.Equals("pendding"))
                    {
                        if (con.Value && con.Key.Equals(t.state.ToLower()))
                        {
                            temp.Add(t);
                        }
                    }
                    else
                    {
                        if (con.Value && con.Key.Equals(convertPriority(t.priority)))
                        {
                            temp.Add(t);
                        }
                    }
                    val.AddRange(temp);
                }
            }
           
            task.Clear();
            task = val;
            task = task.Distinct().ToList();
            initializeListView();
            
        }


        private string convertPriority(int priotiry)
        {
            switch (priotiry)
            {
                case 1: return "high";
                case 2: return "medium";
                case 3: return "low";
                default: return "high";
            }
        }

        private void chkbDone_CheckedChanged(object sender, EventArgs e)
        {
            conditions["done"] = chkbDone.Checked;
            filter();
        }

        private void chkbDoing_CheckedChanged(object sender, EventArgs e)
        {
            conditions["doing"] = chkbDoing.Checked;
            filter();
        }

        private void chkbPendding_CheckedChanged(object sender, EventArgs e)
        {
            conditions["pendding"] = chkbPendding.Checked;
            filter();
        }

        private void chkbHigh_CheckedChanged(object sender, EventArgs e)
        {
            conditions["high"] = chkbHigh.Checked;
            filter();
        }

        private void chkbMedium_CheckedChanged(object sender, EventArgs e)
        {
            conditions["medium"] = chkbMedium.Checked;
            filter();
        }

        private void chkbLow_CheckedChanged(object sender, EventArgs e)
        {
            conditions["low"] = chkbLow.Checked;
            filter();
        }
    }
}
      
 
