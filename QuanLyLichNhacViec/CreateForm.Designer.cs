﻿namespace QuanLyLichNhacViec
{
    partial class CreateFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblTieuDe = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.sfDTDueAt = new Syncfusion.WinForms.Input.SfDateTimeEdit();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rtxtContent = new System.Windows.Forms.RichTextBox();
            this.sfDTCreateAt = new Syncfusion.WinForms.Input.SfDateTimeEdit();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAddTodo = new System.Windows.Forms.Button();
            this.txtAddTodo = new System.Windows.Forms.TextBox();
            this.ckTodo = new System.Windows.Forms.CheckedListBox();
            this.cmtDeleteTodo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmDeleteToDo = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.cboPriority = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.cmtDeleteTodo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTieuDe
            // 
            this.lblTieuDe.AutoSize = true;
            this.lblTieuDe.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTieuDe.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblTieuDe.Location = new System.Drawing.Point(11, 7);
            this.lblTieuDe.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTieuDe.Name = "lblTieuDe";
            this.lblTieuDe.Size = new System.Drawing.Size(63, 18);
            this.lblTieuDe.TabIndex = 0;
            this.lblTieuDe.Text = "Tiêu đề:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(273, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Bắt đầu:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(447, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Kết thúc:";
            // 
            // sfDTDueAt
            // 
            this.sfDTDueAt.DateTimePattern = Syncfusion.WinForms.Input.Enums.DateTimePattern.Custom;
            this.sfDTDueAt.Location = new System.Drawing.Point(519, 6);
            this.sfDTDueAt.Margin = new System.Windows.Forms.Padding(2);
            this.sfDTDueAt.Name = "sfDTDueAt";
            this.sfDTDueAt.Size = new System.Drawing.Size(91, 18);
            this.sfDTDueAt.TabIndex = 6;
            this.sfDTDueAt.ValueChanged += new Syncfusion.WinForms.Input.Events.DateTimeValueChangedEventHandler(this.dueAtChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rtxtContent);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.groupBox1.Location = new System.Drawing.Point(9, 31);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(765, 106);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nội dung";
            // 
            // rtxtContent
            // 
            this.rtxtContent.Location = new System.Drawing.Point(5, 23);
            this.rtxtContent.Margin = new System.Windows.Forms.Padding(2);
            this.rtxtContent.Name = "rtxtContent";
            this.rtxtContent.Size = new System.Drawing.Size(756, 70);
            this.rtxtContent.TabIndex = 0;
            this.rtxtContent.Text = "";
            // 
            // sfDTCreateAt
            // 
            this.sfDTCreateAt.DateTimePattern = Syncfusion.WinForms.Input.Enums.DateTimePattern.Custom;
            this.sfDTCreateAt.Location = new System.Drawing.Point(343, 7);
            this.sfDTCreateAt.Margin = new System.Windows.Forms.Padding(2);
            this.sfDTCreateAt.Name = "sfDTCreateAt";
            this.sfDTCreateAt.Size = new System.Drawing.Size(90, 18);
            this.sfDTCreateAt.TabIndex = 7;
            this.sfDTCreateAt.ValueChanged += new Syncfusion.WinForms.Input.Events.DateTimeValueChangedEventHandler(this.createAtChanged);
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(76, 7);
            this.txtTitle.Margin = new System.Windows.Forms.Padding(2);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(183, 20);
            this.txtTitle.TabIndex = 8;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnAddTodo);
            this.groupBox2.Controls.Add(this.txtAddTodo);
            this.groupBox2.Controls.Add(this.ckTodo);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.groupBox2.Location = new System.Drawing.Point(9, 141);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(765, 222);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Việc cần làm";
            // 
            // btnAddTodo
            // 
            this.btnAddTodo.AutoSize = true;
            this.btnAddTodo.Location = new System.Drawing.Point(657, 189);
            this.btnAddTodo.Name = "btnAddTodo";
            this.btnAddTodo.Size = new System.Drawing.Size(104, 28);
            this.btnAddTodo.TabIndex = 11;
            this.btnAddTodo.Text = "Thêm";
            this.btnAddTodo.UseVisualStyleBackColor = true;
            this.btnAddTodo.Click += new System.EventHandler(this.btnAddTodo_Click);
            // 
            // txtAddTodo
            // 
            this.txtAddTodo.Location = new System.Drawing.Point(5, 191);
            this.txtAddTodo.Name = "txtAddTodo";
            this.txtAddTodo.Size = new System.Drawing.Size(646, 26);
            this.txtAddTodo.TabIndex = 10;
            // 
            // ckTodo
            // 
            this.ckTodo.ContextMenuStrip = this.cmtDeleteTodo;
            this.ckTodo.FormattingEnabled = true;
            this.ckTodo.Location = new System.Drawing.Point(5, 24);
            this.ckTodo.Name = "ckTodo";
            this.ckTodo.Size = new System.Drawing.Size(755, 151);
            this.ckTodo.TabIndex = 0;
            this.ckTodo.SelectedIndexChanged += new System.EventHandler(this.ckTodo_SelectedIndexChanged);
            // 
            // cmtDeleteTodo
            // 
            this.cmtDeleteTodo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmDeleteToDo});
            this.cmtDeleteTodo.Name = "cmtDeleteTodo";
            this.cmtDeleteTodo.Size = new System.Drawing.Size(100, 26);
            // 
            // tsmDeleteToDo
            // 
            this.tsmDeleteToDo.AccessibleRole = System.Windows.Forms.AccessibleRole.Row;
            this.tsmDeleteToDo.Name = "tsmDeleteToDo";
            this.tsmDeleteToDo.Size = new System.Drawing.Size(99, 22);
            this.tsmDeleteToDo.Text = "Xóa";
            this.tsmDeleteToDo.Click += new System.EventHandler(this.tsmDeleteToDo_Click);
            // 
            // btnSave
            // 
            this.btnSave.AutoSize = true;
            this.btnSave.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSave.Location = new System.Drawing.Point(504, 368);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(132, 29);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Lưu";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.AutoSize = true;
            this.btnExit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnExit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnExit.Location = new System.Drawing.Point(642, 368);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(132, 29);
            this.btnExit.TabIndex = 13;
            this.btnExit.Text = "Thoát";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // cboPriority
            // 
            this.cboPriority.FormattingEnabled = true;
            this.cboPriority.Items.AddRange(new object[] {
            "Rất quan trọng",
            "Quan trọng",
            "Trung bình",
            "Thấp"});
            this.cboPriority.Location = new System.Drawing.Point(698, 3);
            this.cboPriority.Name = "cboPriority";
            this.cboPriority.Size = new System.Drawing.Size(71, 21);
            this.cboPriority.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(633, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 18);
            this.label1.TabIndex = 15;
            this.label1.Text = "Ưu tiên";
            // 
            // CreateFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 409);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboPriority);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.sfDTCreateAt);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.sfDTDueAt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblTieuDe);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "CreateFrm";
            this.Text = "Thêm nhắc việc";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.cmtDeleteTodo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTieuDe;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Syncfusion.WinForms.Input.SfDateTimeEdit sfDTDueAt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox rtxtContent;
        private Syncfusion.WinForms.Input.SfDateTimeEdit sfDTCreateAt;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAddTodo;
        private System.Windows.Forms.TextBox txtAddTodo;
        private System.Windows.Forms.CheckedListBox ckTodo;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ComboBox cboPriority;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip cmtDeleteTodo;
        private System.Windows.Forms.ToolStripMenuItem tsmDeleteToDo;
    }
}