﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyLichNhacViec.models;

namespace QuanLyLichNhacViec.DAO
{
    class TodoDAOImpl : TodoDAO
    {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance();

        public TodoDAOImpl()
        {
            databaseHelper.closeConnection();
            databaseHelper.getSqlConnection().openConnection();
        }

        public void addTodo(int id, Todo todo)
        {
            //string state = todo.state == null ? "NOT_DONE" : "DONE";
            string query = string.Format("EXECUTE ADD_TASK_TODO @ID = {0}, @TODO_TITLE = N'{1}',  @TODO_STATE=N'{2}'", id,todo.title,todo.state);
            SqlCommand command = databaseHelper.makeCommand(query);
            command.ExecuteNonQuery();
        }

        public bool deleteTodo(Todo todo)
        {
            string query = string.Format("DELETE TODO WHERE TODO_ID = {0}",todo.id);
            SqlCommand command = databaseHelper.makeCommand(query);
            int result = command.ExecuteNonQuery();
            return result == 1;
        }

        public List<Todo> getAllTodo(int taskId)
        {


            List<Todo> todo = new List<Todo>();
        
            SqlCommand sqlCommand = databaseHelper.makeCommand("select * from TODO where TASK_ID = " + taskId);
            
            SqlDataReader reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                int id = int.Parse(reader["TODO_ID"].ToString());
                string taskID = reader["TASK_ID"].ToString();
                string title = reader["TODO_TITLE"].ToString();
                string state = reader["TODO_STATE"].ToString();

                todo.Add(new Todo(id, taskId, title, state));
            }
            databaseHelper.closeConnection();
            return todo;
        }

        public Todo getTodo(int ID)
        {
            throw new NotImplementedException();
        }

     
        public void updateTodo(int todoId, string state)
        {
            string query = string.Format("EXECUTE UPDATE_TASK_TODO @TODO_ID={0}, @TODO_STATE= N'{1}'", todoId, state);
            databaseHelper.makeCommand(query).ExecuteNonQuery();           
        }
    }
}
