﻿using QuanLyLichNhacViec.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyLichNhacViec.DAO
{
   public interface TodoDAO
    {
        List<Todo> getAllTodo(int taskId);
        void addTodo(int id, Todo todo);
        Todo getTodo(int ID);
        void updateTodo(int taskId, string state);
        bool deleteTodo(Todo todo);
    }
}
