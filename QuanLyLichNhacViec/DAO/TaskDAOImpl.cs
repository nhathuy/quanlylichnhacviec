﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using QuanLyLichNhacViec.models;

namespace QuanLyLichNhacViec.DAO
{
    class TaskDAOImpl : TaskDAO
    {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance();

        public TaskDAOImpl()
        {
            databaseHelper.getSqlConnection().openConnection();
        }


        public void deleteTask(TaskCal task)
        {
            SqlCommand sqlCommand = databaseHelper.makeCommand("DELETE FROM TASK WHERE ID ="+task.id.ToString());
            SqlCommand deleteTodo = databaseHelper.makeCommand("DELETE FROM TODO WHERE TASK_ID = "+task.id.ToString());
            deleteTodo.ExecuteNonQuery();
            sqlCommand.ExecuteNonQuery();
        }

        public List<TaskCal> getAllTasks()
        {   
                List<TaskCal> task = new List<TaskCal>();
                SqlCommand sqlCommand = databaseHelper.makeCommand("select * from TASK");
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    int id = int.Parse(reader["ID"].ToString());
                    string title = reader["TITLE"].ToString();
                    string content = reader["CONTENT"].ToString();
                    DateTime createAt = DateTime.Parse(reader["CREATE_AT"].ToString());
                    DateTime dueAt = DateTime.Parse(reader["DUE_AT"].ToString());
                    string   state = reader["TASK_STATE"].ToString();
                    int priority = int.Parse(reader["TASK_PRIORITY"].ToString());
               
                task.Add(new TaskCal(id, title, content, createAt, dueAt, state, priority));
                }

                foreach(TaskCal val in task)
            {

                List<Todo> todo = new TodoDAOImpl().getAllTodo(val.id);
                val.setTodo(todo);
            }

            return task;
         
        }

        public void updateTask(TaskCal task)
        {
            string creatAt = string.Format("{0}/{1}/{2}", task.createAt.Month, task.createAt.Day, task.createAt.Year);
            string dueAt = string.Format("{0}/{1}/{2}", task.dueAt.Month, task.dueAt.Day, task.dueAt.Year);
            string query = string.Format("EXECUTE UPDATE_TASK_MISS_TODO @ID = N'{0}', @TITLE = N'{1}', @CONTENT = N'{2}', @CREATE_AT = '{3}', @DUE_AT = '{4}', @TASK_STATE = N'{5}', @TASK_PRIORITY = '{6}'",
                task.id, task.title, task.content, creatAt, dueAt,task.state, task.priority);
            databaseHelper.makeCommand(query).ExecuteNonQuery();
        }

        public TaskCal getTask(int ID)
        {
            throw new NotImplementedException();
        }

        public void updateDateOfTask(int taskId, DateTime CreateDate, DateTime DueAt)
        {
            string creatAt = string.Format("{0}/{1}/{2}", CreateDate.Month, CreateDate.Day, CreateDate.Year);
            string dueAt = string.Format("{0}/{1}/{2}", DueAt.Month, DueAt.Day, DueAt.Year);
            string query = string.Format("EXECUTE UPDATE_Date @ID={0}, @CREATE_AT='{1}', @DUE_AT='{2}'", taskId, creatAt, dueAt);
            databaseHelper.makeCommand(query).ExecuteNonQuery();
        }

        public void addTask(TaskCal task)
        {

            string creatAt = string.Format("{0}/{1}/{2}", task.createAt.Month, task.createAt.Day, task.createAt.Year);
            string dueAt = string.Format("{0}/{1}/{2}", task.dueAt.Month, task.dueAt.Day, task.dueAt.Year);
            string query = string.Format("EXECUTE ADD_TASK_MISS_TODO @TITLE = N'{0}', @CONTENT = N'{1}',@CREATE_AT = '{2}',@DUE_AT = '{3}',@TASK_STATE = 'PENDING',@TASK_PRIORITY = {4}",task.title, task.content, creatAt, dueAt, task.priority);
            SqlCommand commad = databaseHelper.makeCommand(query);
            int id = Convert.ToInt32(commad.ExecuteScalar());
            TodoDAOImpl todoDAOImpl = new TodoDAOImpl();
            foreach(Todo val in task.todo)
            {
                todoDAOImpl.addTodo(id, val);
            }
        
        }
    }
}
