﻿using QuanLyLichNhacViec.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuanLyLichNhacViec.DAO
{
    //Data Access Object
    public interface TaskDAO
    {
        List<TaskCal> getAllTasks();
        void addTask(TaskCal task);
        TaskCal getTask(int ID);
        void updateTask(TaskCal task);
        void deleteTask(TaskCal task);
        
    }
}
