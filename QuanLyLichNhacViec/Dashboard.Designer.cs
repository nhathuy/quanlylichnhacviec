﻿namespace QuanLyLichNhacViec
{
    partial class mainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.sfSkinManager1 = new Syncfusion.WinForms.Controls.SfSkinManager(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbChonNgay = new System.Windows.Forms.GroupBox();
            this.sfbtnToday = new Syncfusion.WinForms.Controls.SfButton();
            this.sfCalendar1 = new Syncfusion.WinForms.Input.SfCalendar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAllTask = new System.Windows.Forms.Button();
            this.btnAddTask = new System.Windows.Forms.Button();
            this.listTask = new System.Windows.Forms.ListView();
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.titleCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contentCal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.createAtCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dueAtCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.stateCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.processCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblDanhSachCongViec = new System.Windows.Forms.Label();
            this.pnListTask = new System.Windows.Forms.Panel();
            this.chkbLow = new System.Windows.Forms.CheckBox();
            this.chkbMedium = new System.Windows.Forms.CheckBox();
            this.chkbHigh = new System.Windows.Forms.CheckBox();
            this.chkbDoing = new System.Windows.Forms.CheckBox();
            this.chkbPendding = new System.Windows.Forms.CheckBox();
            this.chkbDone = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPriority = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.dtpSelectDay = new System.Windows.Forms.DateTimePicker();
            this.btnExportTXT = new System.Windows.Forms.Button();
            this.sttLabel = new System.Windows.Forms.StatusStrip();
            this.laTaskStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.laTaskDone = new System.Windows.Forms.ToolStripStatusLabel();
            this.laTaskPendding = new System.Windows.Forms.ToolStripStatusLabel();
            this.laTaskDoing = new System.Windows.Forms.ToolStripStatusLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lvTask = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.gbChonNgay.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnListTask.SuspendLayout();
            this.sttLabel.SuspendLayout();
            this.SuspendLayout();
            // 
            // sfSkinManager1
            // 
            this.sfSkinManager1.Component = null;
            this.sfSkinManager1.Controls = null;
            this.sfSkinManager1.ThemeName = null;
            this.sfSkinManager1.VisualTheme = Syncfusion.Windows.Forms.VisualTheme.Managed;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gbChonNgay);
            this.panel2.Location = new System.Drawing.Point(469, 67);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(339, 450);
            this.panel2.TabIndex = 4;
            // 
            // gbChonNgay
            // 
            this.gbChonNgay.Controls.Add(this.sfbtnToday);
            this.gbChonNgay.Controls.Add(this.sfCalendar1);
            this.gbChonNgay.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.gbChonNgay.Location = new System.Drawing.Point(6, 3);
            this.gbChonNgay.Name = "gbChonNgay";
            this.gbChonNgay.Size = new System.Drawing.Size(333, 444);
            this.gbChonNgay.TabIndex = 1;
            this.gbChonNgay.TabStop = false;
            this.gbChonNgay.Text = "CHỌN NGÀY THÁNG";
            // 
            // sfbtnToday
            // 
            this.sfbtnToday.AccessibleName = "Button";
            this.sfbtnToday.Font = new System.Drawing.Font("Segoe UI Semibold", 9F);
            this.sfbtnToday.Location = new System.Drawing.Point(231, 41);
            this.sfbtnToday.Name = "sfbtnToday";
            this.sfbtnToday.Size = new System.Drawing.Size(96, 28);
            this.sfbtnToday.TabIndex = 1;
            this.sfbtnToday.Text = "TODAY";
            // 
            // sfCalendar1
            // 
            this.sfCalendar1.Culture = new System.Globalization.CultureInfo("en-US");
            this.sfCalendar1.Location = new System.Drawing.Point(0, 75);
            this.sfCalendar1.MinimumSize = new System.Drawing.Size(308, 308);
            this.sfCalendar1.Name = "sfCalendar1";
            this.sfCalendar1.SelectedDate = new System.DateTime(2020, 12, 12, 0, 0, 0, 0);
            this.sfCalendar1.ShowToolTip = true;
            this.sfCalendar1.Size = new System.Drawing.Size(329, 366);
            this.sfCalendar1.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.sfCalendar1.Style.HorizontalSplitterColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.sfCalendar1.Style.VerticalSplitterColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.sfCalendar1.TabIndex = 0;
            this.sfCalendar1.Text = "sfCalendar1";
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.btnAllTask);
            this.panel1.Controls.Add(this.btnAddTask);
            this.panel1.Controls.Add(this.listTask);
            this.panel1.Controls.Add(this.lblDanhSachCongViec);
            this.panel1.Location = new System.Drawing.Point(-61, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(524, 450);
            this.panel1.TabIndex = 3;
            // 
            // btnAllTask
            // 
            this.btnAllTask.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnAllTask.Location = new System.Drawing.Point(333, 49);
            this.btnAllTask.Name = "btnAllTask";
            this.btnAllTask.Size = new System.Drawing.Size(88, 23);
            this.btnAllTask.TabIndex = 5;
            this.btnAllTask.Text = "Tất cả";
            this.btnAllTask.UseVisualStyleBackColor = true;
            // 
            // btnAddTask
            // 
            this.btnAddTask.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnAddTask.Location = new System.Drawing.Point(427, 49);
            this.btnAddTask.Name = "btnAddTask";
            this.btnAddTask.Size = new System.Drawing.Size(88, 23);
            this.btnAddTask.TabIndex = 4;
            this.btnAddTask.Text = "Thêm";
            this.btnAddTask.UseVisualStyleBackColor = true;
            // 
            // listTask
            // 
            this.listTask.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.titleCol,
            this.contentCal,
            this.createAtCol,
            this.dueAtCol,
            this.stateCol,
            this.processCol});
            this.listTask.HideSelection = false;
            this.listTask.Location = new System.Drawing.Point(3, 78);
            this.listTask.Name = "listTask";
            this.listTask.Size = new System.Drawing.Size(518, 369);
            this.listTask.TabIndex = 3;
            this.listTask.UseCompatibleStateImageBehavior = false;
            this.listTask.View = System.Windows.Forms.View.Details;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            this.ID.Width = 26;
            // 
            // titleCol
            // 
            this.titleCol.Text = "Tiêu đề";
            this.titleCol.Width = 79;
            // 
            // contentCal
            // 
            this.contentCal.Text = "Nội dung";
            this.contentCal.Width = 130;
            // 
            // createAtCol
            // 
            this.createAtCol.Text = "Ngày bắt đầu";
            this.createAtCol.Width = 81;
            // 
            // dueAtCol
            // 
            this.dueAtCol.Text = "Ngày kết thúc";
            this.dueAtCol.Width = 89;
            // 
            // stateCol
            // 
            this.stateCol.Text = "Trạng thái";
            this.stateCol.Width = 61;
            // 
            // processCol
            // 
            this.processCol.Text = "Tiến độ";
            this.processCol.Width = 46;
            // 
            // lblDanhSachCongViec
            // 
            this.lblDanhSachCongViec.AutoSize = true;
            this.lblDanhSachCongViec.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblDanhSachCongViec.Location = new System.Drawing.Point(3, 6);
            this.lblDanhSachCongViec.Name = "lblDanhSachCongViec";
            this.lblDanhSachCongViec.Size = new System.Drawing.Size(200, 19);
            this.lblDanhSachCongViec.TabIndex = 0;
            this.lblDanhSachCongViec.Text = "DANH SÁCH CÔNG VIỆC";
            // 
            // pnListTask
            // 
            this.pnListTask.AutoSize = true;
            this.pnListTask.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnListTask.Controls.Add(this.chkbLow);
            this.pnListTask.Controls.Add(this.chkbMedium);
            this.pnListTask.Controls.Add(this.chkbHigh);
            this.pnListTask.Controls.Add(this.chkbDoing);
            this.pnListTask.Controls.Add(this.chkbPendding);
            this.pnListTask.Controls.Add(this.chkbDone);
            this.pnListTask.Controls.Add(this.label2);
            this.pnListTask.Controls.Add(this.lblPriority);
            this.pnListTask.Controls.Add(this.lblState);
            this.pnListTask.Controls.Add(this.dtpSelectDay);
            this.pnListTask.Controls.Add(this.btnExportTXT);
            this.pnListTask.Controls.Add(this.sttLabel);
            this.pnListTask.Controls.Add(this.button1);
            this.pnListTask.Controls.Add(this.button2);
            this.pnListTask.Controls.Add(this.lvTask);
            this.pnListTask.Controls.Add(this.label1);
            this.pnListTask.Location = new System.Drawing.Point(-4, 16);
            this.pnListTask.Name = "pnListTask";
            this.pnListTask.Size = new System.Drawing.Size(836, 547);
            this.pnListTask.TabIndex = 3;
            this.pnListTask.Paint += new System.Windows.Forms.PaintEventHandler(this.pnListTask_Paint);
            // 
            // chkbLow
            // 
            this.chkbLow.AutoSize = true;
            this.chkbLow.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.chkbLow.Location = new System.Drawing.Point(243, 136);
            this.chkbLow.Name = "chkbLow";
            this.chkbLow.Size = new System.Drawing.Size(62, 22);
            this.chkbLow.TabIndex = 21;
            this.chkbLow.Text = "Thấp";
            this.chkbLow.UseVisualStyleBackColor = true;
            this.chkbLow.CheckedChanged += new System.EventHandler(this.chkbLow_CheckedChanged);
            // 
            // chkbMedium
            // 
            this.chkbMedium.AutoSize = true;
            this.chkbMedium.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.chkbMedium.Location = new System.Drawing.Point(243, 108);
            this.chkbMedium.Name = "chkbMedium";
            this.chkbMedium.Size = new System.Drawing.Size(112, 22);
            this.chkbMedium.TabIndex = 20;
            this.chkbMedium.Text = "Bình thường";
            this.chkbMedium.UseVisualStyleBackColor = true;
            this.chkbMedium.CheckedChanged += new System.EventHandler(this.chkbMedium_CheckedChanged);
            // 
            // chkbHigh
            // 
            this.chkbHigh.AutoSize = true;
            this.chkbHigh.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.chkbHigh.Location = new System.Drawing.Point(243, 80);
            this.chkbHigh.Name = "chkbHigh";
            this.chkbHigh.Size = new System.Drawing.Size(103, 22);
            this.chkbHigh.TabIndex = 19;
            this.chkbHigh.Text = "Quan trọng";
            this.chkbHigh.UseVisualStyleBackColor = true;
            this.chkbHigh.CheckedChanged += new System.EventHandler(this.chkbHigh_CheckedChanged);
            // 
            // chkbDoing
            // 
            this.chkbDoing.AutoSize = true;
            this.chkbDoing.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.chkbDoing.Location = new System.Drawing.Point(30, 108);
            this.chkbDoing.Name = "chkbDoing";
            this.chkbDoing.Size = new System.Drawing.Size(131, 22);
            this.chkbDoing.TabIndex = 18;
            this.chkbDoing.Text = "Đang tiến hành";
            this.chkbDoing.UseVisualStyleBackColor = true;
            this.chkbDoing.CheckedChanged += new System.EventHandler(this.chkbDoing_CheckedChanged);
            // 
            // chkbPendding
            // 
            this.chkbPendding.AutoSize = true;
            this.chkbPendding.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.chkbPendding.Location = new System.Drawing.Point(30, 136);
            this.chkbPendding.Name = "chkbPendding";
            this.chkbPendding.Size = new System.Drawing.Size(96, 22);
            this.chkbPendding.TabIndex = 17;
            this.chkbPendding.Text = "Đang chờ";
            this.chkbPendding.UseVisualStyleBackColor = true;
            this.chkbPendding.CheckedChanged += new System.EventHandler(this.chkbPendding_CheckedChanged);
            // 
            // chkbDone
            // 
            this.chkbDone.AutoSize = true;
            this.chkbDone.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.chkbDone.Location = new System.Drawing.Point(30, 80);
            this.chkbDone.Name = "chkbDone";
            this.chkbDone.Size = new System.Drawing.Size(105, 22);
            this.chkbDone.TabIndex = 16;
            this.chkbDone.Text = "Hoàn thành";
            this.chkbDone.UseVisualStyleBackColor = true;
            this.chkbDone.CheckedChanged += new System.EventHandler(this.chkbDone_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(522, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 18);
            this.label2.TabIndex = 15;
            this.label2.Text = "Chọn ngày tháng";
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblPriority.Location = new System.Drawing.Point(240, 38);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(81, 18);
            this.lblPriority.TabIndex = 14;
            this.lblPriority.Text = "Độ ưu tiên";
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblState.Location = new System.Drawing.Point(27, 38);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(76, 18);
            this.lblState.TabIndex = 12;
            this.lblState.Text = "Trạng thái";
            // 
            // dtpSelectDay
            // 
            this.dtpSelectDay.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.dtpSelectDay.Location = new System.Drawing.Point(515, 75);
            this.dtpSelectDay.Name = "dtpSelectDay";
            this.dtpSelectDay.Size = new System.Drawing.Size(216, 26);
            this.dtpSelectDay.TabIndex = 10;
            // 
            // btnExportTXT
            // 
            this.btnExportTXT.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnExportTXT.Location = new System.Drawing.Point(703, 154);
            this.btnExportTXT.Name = "btnExportTXT";
            this.btnExportTXT.Size = new System.Drawing.Size(126, 30);
            this.btnExportTXT.TabIndex = 8;
            this.btnExportTXT.Text = "Xuất danh sách";
            this.btnExportTXT.UseVisualStyleBackColor = true;
            this.btnExportTXT.Click += new System.EventHandler(this.btnExportTXT_Click);
            // 
            // sttLabel
            // 
            this.sttLabel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laTaskStatus,
            this.laTaskDone,
            this.laTaskPendding,
            this.laTaskDoing});
            this.sttLabel.Location = new System.Drawing.Point(0, 525);
            this.sttLabel.Name = "sttLabel";
            this.sttLabel.Size = new System.Drawing.Size(836, 22);
            this.sttLabel.TabIndex = 6;
            this.sttLabel.Text = "statusStrip1";
            // 
            // laTaskStatus
            // 
            this.laTaskStatus.Name = "laTaskStatus";
            this.laTaskStatus.Size = new System.Drawing.Size(131, 17);
            this.laTaskStatus.Text = "toolStripStatusLabel1";
            // 
            // laTaskDone
            // 
            this.laTaskDone.Name = "laTaskDone";
            this.laTaskDone.Size = new System.Drawing.Size(74, 17);
            this.laTaskDone.Text = "laTaskDone";
            // 
            // laTaskPendding
            // 
            this.laTaskPendding.Name = "laTaskPendding";
            this.laTaskPendding.Size = new System.Drawing.Size(131, 17);
            this.laTaskPendding.Text = "toolStripStatusLabel1";
            // 
            // laTaskDoing
            // 
            this.laTaskDoing.Name = "laTaskDoing";
            this.laTaskDoing.Size = new System.Drawing.Size(131, 17);
            this.laTaskDoing.Text = "toolStripStatusLabel1";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button1.Location = new System.Drawing.Point(515, 154);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 30);
            this.button1.TabIndex = 5;
            this.button1.Text = "Tất cả";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnAllTask_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button2.Location = new System.Drawing.Point(609, 154);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(88, 30);
            this.button2.TabIndex = 4;
            this.button2.Text = "Thêm";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnAddTask_Click);
            // 
            // lvTask
            // 
            this.lvTask.AutoArrange = false;
            this.lvTask.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.lvTask.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lvTask.HideSelection = false;
            this.lvTask.Location = new System.Drawing.Point(1, 190);
            this.lvTask.Name = "lvTask";
            this.lvTask.Size = new System.Drawing.Size(828, 334);
            this.lvTask.TabIndex = 3;
            this.lvTask.UseCompatibleStateImageBehavior = false;
            this.lvTask.View = System.Windows.Forms.View.Details;
            this.lvTask.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            this.lvTask.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvTask_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 26;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Tiêu đề";
            this.columnHeader2.Width = 79;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Nội dung";
            this.columnHeader3.Width = 130;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Ngày bắt đầu";
            this.columnHeader4.Width = 81;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Ngày kết thúc";
            this.columnHeader5.Width = 89;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Trạng thái";
            this.columnHeader6.Width = 61;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Tiến độ";
            this.columnHeader7.Width = 46;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(293, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(252, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "DANH SÁCH CÔNG VIỆC";
            // 
            // mainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(827, 566);
            this.Controls.Add(this.pnListTask);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "mainFrm";
            this.Text = "Quản Lí Lịch Nhắc Việc";
            this.Load += new System.EventHandler(this.formLoad);
            this.panel2.ResumeLayout(false);
            this.gbChonNgay.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnListTask.ResumeLayout(false);
            this.pnListTask.PerformLayout();
            this.sttLabel.ResumeLayout(false);
            this.sttLabel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Syncfusion.WinForms.Controls.SfSkinManager sfSkinManager1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox gbChonNgay;
        private Syncfusion.WinForms.Controls.SfButton sfbtnToday;
        private Syncfusion.WinForms.Input.SfCalendar sfCalendar1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAllTask;
        private System.Windows.Forms.Button btnAddTask;
        private System.Windows.Forms.ListView listTask;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader titleCol;
        private System.Windows.Forms.ColumnHeader contentCal;
        private System.Windows.Forms.ColumnHeader createAtCol;
        private System.Windows.Forms.ColumnHeader dueAtCol;
        private System.Windows.Forms.ColumnHeader stateCol;
        private System.Windows.Forms.ColumnHeader processCol;
        private System.Windows.Forms.Label lblDanhSachCongViec;
        private System.Windows.Forms.Panel pnListTask;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListView lvTask;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip sttLabel;
        private System.Windows.Forms.ToolStripStatusLabel laTaskStatus;
        private System.Windows.Forms.ToolStripStatusLabel laTaskDone;
        private System.Windows.Forms.ToolStripStatusLabel laTaskPendding;
        private System.Windows.Forms.ToolStripStatusLabel laTaskDoing;
        private System.Windows.Forms.Button btnExportTXT;
        private System.Windows.Forms.DateTimePicker dtpSelectDay;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label lblPriority;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkbLow;
        private System.Windows.Forms.CheckBox chkbMedium;
        private System.Windows.Forms.CheckBox chkbHigh;
        private System.Windows.Forms.CheckBox chkbDoing;
        private System.Windows.Forms.CheckBox chkbPendding;
        private System.Windows.Forms.CheckBox chkbDone;
    }
}

