﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyLichNhacViec
{
    //lớp singletone
    public class DatabaseHelper
    {
        static DatabaseHelper instance;
        private string connectionString;
        private SqlConnection sqlConnection;
        private SqlCommand sqlCommand;



        public DatabaseHelper()
        {
            this.connectionString = "server=DESKTOP-HQE2TAG\\SQLEXPRESS; database = QuanLyLichNhacViec; Integrated Security = true; ";

        }
        /* Sử dụng singleton để không phải tạo lại database helper và giữ các biến đó trong suốt quá 
         * trình hoạt động của chương trình
         * 
         */

        public static DatabaseHelper getInstance()
        {
            if (instance == null)
            {
                instance = new DatabaseHelper();
            }
            return instance;
        }

        /*Khởi tạo sql connection và return về Database helper để tiếp tục gọi tới các function khác nhanh hơn
         */
        public DatabaseHelper getSqlConnection()
        {
          
                this.sqlConnection = new SqlConnection(this.connectionString);
            return this;
        }

        /*Khởi tạo sql command và gán query cho sql command
         * Sử dụng dạng singletone để không phải khởi tạo lại sqlcommand 
         */
        public SqlCommand makeCommand(string command)
        {
            if(this.sqlConnection == null)
            {
                getSqlConnection();
                this.openConnection();
            }
            this.sqlCommand = this.sqlConnection.CreateCommand();
            this.sqlCommand.CommandText = command;
            return this.sqlCommand;
        }

        /*Mở kết nối 
         */
        public DatabaseHelper openConnection()
        {
            this.sqlConnection.Open();
            return this;

        }

        /*Đóng kết nối
         */
        public void closeConnection()
        {
            if(this.sqlConnection != null)
            {
                
                this.sqlConnection.Close();
                this.sqlConnection = null;

            }

        }

        // Giải phóng kết nối tới database
        public void dispose()
        {
            this.sqlConnection.Dispose();

        }
    }
}
