﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyLichNhacViec.models
{
   public class Todo
    {
        public int taskId;
        public string title;
        public string state;
        public int id;

        public Todo()
        {

        }

        public Todo(int id,int taskId, string title, string state)
        {
            this.taskId = taskId;
            this.title = title;
            this.state = state;
            this.id = id;
        }

        public Todo(string title)
        {
      
            this.title = title;
        }


        public override string ToString()
        {
            return string.Format("{0}_{1}_{2}_{3}",this.id, this.taskId, this.title, this.state);
        }
    }

}
