﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyLichNhacViec.models
{
   public class TaskCal
    {
        public int id { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public DateTime createAt { get; set; }
        public DateTime dueAt { get; set; }
        public string state { get; set; }
        public int priority { get; set; }
        public List<Todo> todo { get; set; }


        public TaskCal()
        {

        }

        public TaskCal(int id, string title, string content, DateTime createAt, DateTime dueAt, string state, int priority, List<Todo> todo)
        {
            this.id = id;
            this.title = title;
            this.content = content;
            this.createAt = createAt;
            this.dueAt = dueAt;
            this.state = state;
            this.priority = priority;
            this.todo = todo;
        }

        public TaskCal(int id, string title, string content, DateTime createAt, DateTime dueAt, string state, int priority)
        {
            this.id = id;
            this.title = title;
            this.content = content;
            this.createAt = createAt;
            this.dueAt = dueAt;
            this.state = state;
            this.priority = priority;
        }
        public TaskCal( string title, string content, DateTime createAt, DateTime dueAt, int priority)
        {
          
            this.title = title;
            this.content = content;
            this.createAt = createAt;
            this.dueAt = dueAt;
            this.state = state;
            this.priority = priority;
        }
     
        //Gán danh sách to do sau khi lấy lên
        public void setTodo(List<Todo> todo)
        {
            this.todo = todo;
            setState();
        }

        private void setState()
        {

            int done = doneQuantity();
            int total = this.todo.Count;
  
            if (done == total && total > 0)
            {
                this.state = "DONE";

            }
            else if (done == 0 && total > 0)
            {
                this.state = "PENDDING";
            }
            else
            {
                this.state = "DOING";
            }
        }

        //xóa một todo trong danh sách
        public void removeTodo(string title)
        {
            Todo t = todo.Single(p => p.title == title);
            this.todo.Remove(t);
        }


        public string[] toRow()
        {
            string[] row =  {this.id.ToString(), this.title, this.content, this.createAt.ToShortDateString(), this.dueAt.ToShortDateString(), this.state, doneYet() };
            return row;
        }

        private string doneYet()
        {
            int done = this.todo.Count(p => p.state == "DONE");
            return string.Format("{0}/{1}",done,this.todo.Count());
        }

        private int doneQuantity()
        {
           return  this.todo.Count(p => p.state == "DONE");
        }

        public string toExport()
        {
            String listTodo = "";
            foreach(Todo val in this.todo)
            {
                listTodo += val.ToString() + ";";
            }

            string task = string.Format("{0}\t{2}\t{3}\t{4}\t{5}\t{6}\n", this.id, this.title, this.content, this.createAt.ToString(), this.dueAt.ToString(), this.priority,listTodo);

            return task;
        }
    }


}
