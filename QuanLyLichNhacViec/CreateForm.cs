﻿using QuanLyLichNhacViec.DAO;
using QuanLyLichNhacViec.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyLichNhacViec
{

    enum TypeOpen
    {
           create, detail
    }

    public partial class CreateFrm : Form
    {
        DateTime dueAt = DateTime.Today;
        DateTime createAt = DateTime.Today;
        List<Todo> todoList = new List<Todo>();
        List<TaskCal> taskList = new List<TaskCal>();
        TaskCal task;
        int taskID;
        bool dutAtIsChange = false;
        private TypeOpen typeOpen;

        bool isFistCall = true;

        public AsyncCallback closeform;

        public CreateFrm(AsyncCallback callback)
        {
            typeOpen = TypeOpen.create;
            this.closeform = callback;
            InitializeComponent();
            isFistCall = false;
        }


        public CreateFrm()
        {
            typeOpen = TypeOpen.create;
            InitializeComponent();
            isFistCall = false;
        }

        public CreateFrm(TaskCal task, AsyncCallback callback)
        {
            this.closeform = callback;
            typeOpen = TypeOpen.detail;
            InitializeComponent();
            this.task = task;
            this.taskID = task.id;
            txtTitle.Text = task.title;
            sfDTCreateAt.Value = task.createAt;
            sfDTDueAt.Value = task.dueAt;
            rtxtContent.Text = task.content;
            cboPriority.Text = task.priority.ToString();
            todoList = task.todo;
            foreach(Todo val in task.todo)
            {
                bool ischeck = val.state == "DONE";
                ckTodo.Items.Add(val.title, ischeck);
            }
            isFistCall = false;
            
        }




        private void btnSave_Click(object sender, EventArgs e)
        {
            string tasktitle = txtTitle.Text;
            string content = rtxtContent.Text;
            string priorityVal = cboPriority.Text;
            int priority = 1;
            int.TryParse(priorityVal, out priority);

            if (String.IsNullOrEmpty(tasktitle))
            {
                MessageBox.Show("Tiêu đề không được bỏ trống");
                return;
            }

            if (sfDTCreateAt.Value == this.createAt)
            {
                dueAt = DateTime.Today;
            }

            TaskCal task = new TaskCal(tasktitle,content,this.createAt,dueAt,priority);
            if(typeOpen == TypeOpen.create)
            {
                task.setTodo(todoList);
                TaskDAOImpl taskDAOImpl = new TaskDAOImpl();
                taskList = taskDAOImpl.getAllTasks();
                var getIDTask= taskList.FirstOrDefault(x => x.title == tasktitle);
                
                if (getIDTask == null)
                {                   
                    taskDAOImpl.addTask(task);
                    MessageBox.Show("Thêm thành công!");
                    if (closeform != null)
                    {
                        closeform.Invoke(null);
                    }

                    this.Dispose();
                    return;
                }
                else
                {
                    int IDTask = getIDTask.id;
                    var checkIDTask = taskList.FirstOrDefault(x => x.id == IDTask);
                    var temp = taskList.FirstOrDefault(x => x.createAt == this.createAt && x.dueAt==dueAt);
                    if (checkIDTask == null && temp == null)
                    {
                        taskDAOImpl.updateDateOfTask(checkIDTask.id,this.createAt,dueAt);
                        
                        MessageBox.Show("Cập nhật ngày thành công!");
                    }
                    if (checkIDTask != null && temp == null)
                    {
                        if (sfDTDueAt.Value < this.createAt)
                        {
                            sfDTCreateAt.Value = DateTime.Today;
                            MessageBox.Show("Ngày tạo phải trước ngày kết thúc!");
                        }
                        else
                        {
                            taskDAOImpl.addTask(task);
                            MessageBox.Show("Thêm thành công!");
                        }
                    }
                    else
                    {                        
                        MessageBox.Show("Task đã có! Vui lòng chỉnh sửa !");
                    }
                }                              
            }
            //detail
            else
            {
                
                TodoDAOImpl todoDAOImpl = new TodoDAOImpl();
                Test();
                TaskDAOImpl taskDAOImpl = new TaskDAOImpl();
                taskList = taskDAOImpl.getAllTasks();
                foreach (Todo val in todoList)
                {
                    var checkIDTask = taskList.FirstOrDefault(x => x.id == val.taskId);
                    if (checkIDTask != null)
                    {
                        if (sfDTDueAt.Value >= this.createAt)
                        {
                            dueAt= DateTime.Parse(sfDTDueAt.Value.ToString());
                            //taskDAOImpl.updateDateOfTask(checkIDTask.id, this.createAt, dueAt);
                            task.id = checkIDTask.id;
                            task.createAt = this.createAt;
                            task.dueAt
                                 = dueAt;
                            taskDAOImpl.updateTask(task);
                        }
                        else
                        {                          
                            MessageBox.Show("Ngày tạo phải trước ngày kết thúc!");
                            break ;
                        }
                    }
                    if (containTodoID(val.taskId)==true){
                        var checkID = todoList.FirstOrDefault(x => x.taskId == val.taskId);
                        if (checkID != null)
                        {
                            if (val.state == "DONE")
                            {
                                todoDAOImpl.updateTodo(val.id, "DONE");                              
                            }
                            else
                            {
                                todoDAOImpl.updateTodo(val.id, "NOT_DONE");                               
                            }
                        } 

                    }
                    else
                    {
                      todoDAOImpl.addTodo(this.taskID, val);  
                    }
                
                }
                MessageBox.Show("Cập nhật thành công");
                if (closeform != null)
                {
                    closeform.Invoke(null);
                }

                this.Dispose();
            }

        }


        private bool containTodoID(int id)
        {
            if(id != 0)
            {
                var temp=task.todo.FirstOrDefault(t => t.taskId == id);
                if (temp != null)
                {
                    return true;
                }
            }
            return false;
       
        }

        private void createAtChanged(object sender, Syncfusion.WinForms.Input.Events.DateTimeValueChangedEventArgs e)
        {
            if (!isFistCall) {
            var newDate = e.NewValue;
            var oldDate = e.OldValue;

            if (newDate < DateTime.Today)
            {
                MessageBox.Show("Ngày tạo không thể trước ngày hiện tại");
                sfDTCreateAt.Value = DateTime.Today;
            }

            if (newDate == null)
            {
                createAt = oldDate.Value;
            }
            else if (!dutAtIsChange && newDate > dueAt)
            {
                MessageBox.Show("Ngày tạo không thể sau ngày kết thúc");

                sfDTCreateAt.Value = createAt;
            }
            else
            {
                createAt = newDate.Value;

            }
            
        }
        }

        private void dueAtChanged(object sender, Syncfusion.WinForms.Input.Events.DateTimeValueChangedEventArgs e)
        {
            
                var newDate = e.NewValue;
                var oldDate = e.OldValue;
                dutAtIsChange = true;


                if (newDate < createAt)
                {
                    MessageBox.Show("Ngày kết thúc không thể trước ngày tạo");
                    sfDTDueAt.Value = createAt;
                }

                if (newDate == null)
                {
                    dueAt = createAt;
                }
                else
                {
                    dueAt = newDate.Value;
                }
   
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if(closeform != null)
            {
                closeform.Invoke(null);
            }
      
            this.Dispose();
            
        }

        private void btnAddTodo_Click(object sender, EventArgs e)
        {
            string todo = txtAddTodo.Text;
            if(todo==null || todo == "")
            {
                MessageBox.Show("Nhập tên công việc");
                return;
            }
            Todo newTodo = new Todo(todo);
            if (isExist(newTodo))
            {
                MessageBox.Show("Công việc đã tồn tại");
                return;
            }
            int count = ckTodo.Items.Count;
            ckTodo.Items.Insert(count,todo);
            todoList.Add(newTodo);
            if(typeOpen == TypeOpen.detail)
            {
                ///task.todo.Add(new Todo(todo));
            }
            txtAddTodo.Clear();
        }

        private bool isExist(Todo todo)
        {
            if (todo.id == 0) return false;
            List<Todo> todos = task.todo.Where(p => p.title == todo.title).ToList();

            return todos.Count > 0;
        }


        private void tsmDeleteToDo_Click(object sender, EventArgs e)
        {
            string title = ckTodo.Items[ckTodo.SelectedIndex].ToString();
            Todo selectTodo = findTodoByTitle(title);
            TodoDAOImpl todoDAOImpl = new TodoDAOImpl();
            bool result = todoDAOImpl.deleteTodo(selectTodo);
            ckTodo.Items.Remove(title);
            task.removeTodo(title);
        }

    

        private Todo findTodoByTitle(string title) {

            Todo result = null;

            if(typeOpen == TypeOpen.create)
            {
                result = this.todoList.Single(p=> p.title.Equals(title));
            }
            else
            {
                try
                {
                    result = task.todo.Single(p => p.title.Equals(title));
                }
                catch (Exception e)
                {

                }
      
            }

            return result;
        }


        void Test()
        {
            int index = -1;
            for (int i = 0; i < ckTodo.Items.Count; i++)
            {
                if (ckTodo.GetItemChecked(i) == true)
                {
                    foreach (Todo val in todoList)
                    {
                        if (val.title == ckTodo.Items[i].ToString())
                        {
                            index = todoList.IndexOf(val);
                            todoList[index].state = "DONE";
                        }
                    }
                }
                else
                {
                    foreach (Todo val in todoList)
                    {
                        if (val.title == ckTodo.Items[i].ToString())
                        {
                            index = todoList.IndexOf(val);
                            todoList[index].state = "NOT_DONE";
                        }
                    }
                }

            }
        }

        private void ckTodo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
